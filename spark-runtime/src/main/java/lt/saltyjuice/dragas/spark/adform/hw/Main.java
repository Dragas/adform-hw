package lt.saltyjuice.dragas.spark.adform.hw;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions$;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.types.DataTypes;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeoutException;

public class Main {
    public static void main(String[] args) throws TimeoutException, StreamingQueryException {
        SparkSession session = SparkSession.builder().appName("adform homework").getOrCreate();
        session.sqlContext().setConf("spark.sql.streaming.schemaInference", "true");
        String userAgentString = args[0];
        Dataset<Row> impressions = aggregateParquetDataSetByHour(session, "./raw_data/impressions*", userAgentString, "impressions");
        Dataset<Row> clicks = aggregateParquetDataSetByHour(session, "./raw_data/clicks*", userAgentString, "clicks");
        Dataset<Row> joined = impressions.join(clicks, "hour", "left");
        Dataset<Row> minMaxAggregates = joined.agg(functions$.MODULE$.min("hour"), functions$.MODULE$.max("hour"));
        Instant min = minMaxAggregates.first().getTimestamp(0).toInstant();
        Instant max = minMaxAggregates.first().getTimestamp(1).toInstant().plus(1, ChronoUnit.HOURS);
        Dataset<Row> timeseries = generateTimeIntervalSeries(session, min, max, Duration.ofHours(1));
        joined = joined.join(timeseries, "hour", "right")
                .na()
                .fill(0, new String[]{"clicks", "impressions"});
        joined
                .select(functions$.MODULE$.struct("hour", "clicks", "impressions").as("row"))
                .select(org.apache.spark.sql.protobuf.functions$.MODULE$.to_protobuf(functions$.MODULE$.col("row"), "ImpressionClicksByHour"))
                .write()
                .format("kafka")
                .option("kafka.bootstrap.servers", System.getenv("KAFKA"))
                .option("topic", userAgentString)
                .save();
    }

    public static Dataset<Row> aggregateParquetDataSetByHour(SparkSession session, String source, String userAgent, String countColumnName) throws TimeoutException {
        return session
                .read()
                .parquet(source)
                .filter(functions$.MODULE$.col("device_settings.user_agent").equalTo(functions$.MODULE$.lit(userAgent)))
                .withColumn("creation_time_ts", functions$.MODULE$.col("creation_time_local").divide(1000).cast(DataTypes.TimestampType))
                .withColumn("hour", functions$.MODULE$.date_trunc("hour", functions$.MODULE$.col("creation_time_ts")))
                //.withWatermark("hour", "1 hour")
                .groupBy("hour")
                .agg(functions$.MODULE$.count("*").alias(countColumnName));
    }

    public static Dataset<Row> generateTimeIntervalSeries(SparkSession session, Instant start, Instant end, Duration step) {
        return session
                .range(start.toEpochMilli() / 1000, end.toEpochMilli() / 1000, step.toSeconds())
                .toDF("timestamp_column")
                .select(functions$.MODULE$.date_trunc(
                                "hour",
                                functions$.MODULE$.col("timestamp_column").cast(DataTypes.TimestampType)
                        ).as("hour")
                );
    }
}
