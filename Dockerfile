FROM docker.io/azul/zulu-openjdk:17.0.8.1-17.44.53 as builder
RUN mkdir /app
WORKDIR /app
# folder contains trash like the jdk, hadoop home and other noncomittables
# so copy only relevant bits one by one
COPY ./pom.xml .
COPY ./spark-runtime/pom.xml ./spark-runtime/pom.xml
COPY ./spark-runtime/src/main/java/lt/saltyjuice/dragas/spark/adform/hw/Main.java ./spark-runtime/src/main/java/lt/saltyjuice/dragas/spark/adform/hw/Main.java
COPY ./.mvn .
COPY ./mvnw ./mvnw
RUN ./mvnw clean package

FROM docker.io/library/spark:3.5.0-scala2.12-java17-python3-ubuntu as runtime
RUN mkdir /app
WORKDIR /app
COPY --from=builder ./spark-runtime/target/spark-runtime-1.0-SNAPSHOT.jar .
ENTRYPOINT /opt/spark/sbin/spark-submit --master $SPARK_MASTER --packages org.apache.spark:spark-protobuf_2.12:3.5.0,org.apache.spark:spark-sql-kafka-0-10_2.12:3.5.0 --deploy-mode client --class lt.saltyjuice.dragas.spark.adform.hw.Main --name Main2 ./spark-runtime-1.0-SNAPSHOT.jar